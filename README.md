# Kratek vodič skozi Git

## Kaj je git?

Git je program za vodenje različic (revision control system), s katerim skupina
programerjev sodeluje pri pisanju programske opreme. Git hrani zgodovino sprememb, omogoča
vzporedno delo na večih različicah (vejah) izvorne kode in na sploh skrbi, da imajo
programerji usklajene različice.

Git lahko uporabljamo tudi za druge skupne projekte, kot je pisanje knjig in zapiskov
v TeXu. V veliko pomoč je že enemu uporabniku, ki uporablja več računalnikov, saj mu tako
ni treba prenašati dokumentov sem in tja s primitivnimi metodami, kot so elektronska pošta,
USB ključki in Dropbox.

## Kaj potrebujem za delo z git?

### Programska oprema

Na šolskih računalnikih sta že nameščena msysGit in TortoiseGit. Za delo doma pa si
namestite naslednjo opremo:

* OS X: `git` je že nameščen
* Windows: [msysGit](http://msysgit.github.io), za bolj udobno uporabo pa
  [TortoiseGit](https://code.google.com/p/tortoisegit/)
* Linux: namestite si paket `git` ali kakorkoli se mu že reče na vaši distribuciji

Če uporabljate Eclipse, si namestite plugin [EGit](http://eclipse.org/egit/). Ta je
nameščen tudi na Eclipse v računalniških učilnicah. PyCharm ima podporo za git že
vgrajeno.

### Uporabniški račun na BitBucket

Delali bomo z repozitoriji na [BitBucket](https://bitbucket.org/), zato si tam ustvarite
uporabniški račun, če ga še nimate.

Zaposleni na FMF imajo dostop do [privatnega FMF strežnika](http://git.fmf.uni-lj.si/),
zunanji sodelavci pa ga lahko uporabljajo, če imajo Google account.

## Delo z git

### Mini demo

1. Prijavite se na BitBucket.

2. Ustvarite [nov repozitorij](https://bitbucket.org/repo/create).
    * Repozitorij naj bo javen.
    * Obkljukajte opciji `Issue tracking` in `Wiki`.
    * Izberite programski jezik.

3. Ustvarjeni repozitorij klonirajte na svoj računalnik.

4. V repozitorij dodajte datoteko in vanjo nekaj napišite.

5. Naredite `commit`.

6. Naredite `push`.

7. Datoteka bi se morala pojaviti na strežniku.

8. Pobrišite repozitorij na svojem računalniku.

9. Še enkrat ga klonirajte. Ničesar niste izgubili!

### Igrica Snake

#### Fork

Naredite *fork* repozitorija [`ninofmf/snakes`](https://bitbucket.org/ninofmf/snakes)
(opcija "Fork" v meniju na levi). S tem boste dobili svojo različico repozitorija.

#### Clone

Klonirajte **svojo različico** repozitorija k sebi, se pravi ustrezni naslov repozitorija
bo `https://uporabnik@bitbucket.org/uporabnik/snakes.git` kjer `uporabnik` **ni** `ninofmf`.

#### Dodajte svojo kačo

Igrici dodajte svojo kačo:

1. Izberite ime svoje kače (pomagajte si s [tem
   spiskom](http://en.wikipedia.org/wiki/List_of_snakes_by_common_name)), recimo da ste
   izbrali "Black Adder".

2. Datoteko `bolivian_anaconda.py` prekopirajte v `black_adder.py` (oziroma kakorkoli je že
   ime vaši kači).

3. Popravite `black_adder.py` in jo ne pozabite dodati v git repozitorij z

          git add black_adder.py

4. Svojo kačo dodajte v spisek kač `SNAKES` v datoteki `game.py`.

5. Preizkusite igrico, tako da poženete `game.py`.

#### Commit

Ko ste zadovoljni s spremembami, jih dodajte v repozitorij:

    git commit -m "opis sprememb"

#### Push

Spremembe pošljite na GitHub:

    git push

#### Pull

Izberite različico enega od sošolcev (kliknite na `Forks` na strani
[https://bitbucket.org/ninofmf/snakes](https://bitbucket.org/ninofmf/snakes) za seznam
uporabnikov) in njegove spremembe prenesite k sebi. Denimo, da ste izbrali udeleženca z
uporabniškim imenom `lolek`:

    git remote add lolek https://uporabnik@bitbucket.org/lolek/snakes.git
    git pull lolek master

Sedaj imate tudi `lolek`ove kače!

#### Pull request

Svoje spremembe predlagajte za vključitev v glavno različico, ki jo ima uporabnik
`ninofmf`. Na svoji različici naredite *pull request* (izbira "pull requests" na
menuju na desni, nato gumb "new pull request"). Z nekaj sreče bo uporabnik `ninofmf`
sprejel vaše spremembe; lahko pa se tudi odloči, da jih bo zavrnil.

#### Pull iz `upstream`

Spremembe, ki jih bo naredil `ninofmf` (sprejel bo kopico pull requestov) prenesite v
svojo različico:

    git remote add upstream https://uporabnik@bitbucket.org/ninofmf/snakes.git
    git pull upstream master

Sedaj imate kače vseh sošolcev!


## Disclaimer
To gradivo je priredba računalniške delavnice o Git-u, ki jo je izvedel prof. Andrej Bauer
in je potekala 28. novembra 2014 na Fakulteti za matematiko in fiziko, Univerza v Ljubljani.
